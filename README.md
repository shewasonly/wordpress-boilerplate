Wordpress Boilerplate
===================

Contents
----------------
- Overview
- Instructions
- Usage
- Deployment

Overview
----------------
Author: Jonny Thaw

Release version of my newest boilerplate, for a default wordpress theme.

It uses gulp to automate all of the distribution files. It currently handles these things to do with files:

- **JS** - Minifies all of the scripts, concatenates them into scripts.js and moves them to assets
- **CSS** - Uses sass to compile the files and then minify them into assets. Also uses bourbon and neat for frameworks
- **FONT** - Doesn't really do anything special with fonts, but it does just move them the css folder in assets

It also has a reset in there too.

There is also a blank composer.json file for any packagist happenings.

Instructions
-------------------

To use this repository fully, you will need a few things installed on your computer first of all. Do these in order, because things that follow may require things that came before.

####Git

This is the heartbeat for all of this operation. Git is a very valuable thing to learn about if you don't know already and is in fact the thing that will be powering you retrieving all of these files. If you get the chance, read up about Git, because I am not very good at describing it.

####Node.js

This is so that you can use the terminal commands necessary to access NPM (Node package manager) and install the packages that are required to run gulp and the build scripts. If this sounds all like craziness, I apologise but it will become easy as soon as you've done it.

####Gulp

This is the build tool that does loads of cool things to the files ready for production. You install this via Node and NPM which is why you NEED to have node.js installed first. To download this, open up your _Terminal/CMD_ application and type in `npm install -g gulp` (you may have to run it as 'sudo' and you'll then have to enter your password for this).

####Ruby (Non Mac only)

Theres a potential you need to download this, but more likely not. This is the language that runs _Sass_ and subsequently _bourbon_ and _neat_.

####Sass

This is another tool that is installed over command line. Just run this in _Terminal/CMD_ and you're away `gem install sass` (again this may need to be ran with 'sudo' at the start of your command)

####Bourbon and Neat

I'm not totally sure if you need these installed, but its certainly good measure. Run these two commands seperately and you should be ready - `gem install bourbon` & `gem install neat`

####Composer (optional)

Composer is great, but its something you won't necessarily need unless you do something a bit more advanced with PHP. It is a valuable thing to get and learn how to use, because it turns PHP into something more respectable like node.js or ruby that we used before. I would definitely point you towards this bit of information for installing Composer <https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx> as it has the most in depth guide for it. _BUT_ if you are working on a mac it shouldn't be too hard, just run these commands:

```
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
```

####LiveReload (optional)

This is a very handy tool for doing front end work and works beautifully with Gulp and the gulpfile.js in this repository. You install this as extensions to your browser, so for instance google chrome's extension is found here: <https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei>




#####Checklist of downloads

- **Git** <https://git-scm.com/download>
- **node.js** <https://nodejs.org/en/download/>
- **gulp** (installed via command line) <https://www.npmjs.com/package/gulp>
- **Ruby** <https://www.ruby-lang.org/en/downloads/>
- **Sass** (installed via command line) <https://rubygems.org/gems/sass/versions/3.4.19>
- **Bourbon and Neat** (installed via command line) <http://bourbon.io/>
- **MAMP** <https://www.mamp.info/en/downloads/>
- **Composer** <https://getcomposer.org/download/>
- **LiveReload** <https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei>


Usage
=====

So now that we have all of this installed, we can start working with these files. This may have seemed like a lot of setup, but it was a one time thing getting all of these and now we can use all of their powers indefinitely now.

So now what we need to do is open up Terminal (There are other command line applications on other OS's but for this I am going to presume you are working on a mac) and we want to navigate to the folder that our wordpress is installed. So lets assume a few things to help with these instructions:

- Our project is called **Portfolio**
- We are using MAMP to create a local server (so therefore we are working in the folder `/Applications/MAMP/htdocs/`)
- We have already installed wordpress in a folder called **Portfolio**

So what we want to do is in terminal, change to the directory of our site like so `cd /Applications/MAMP/htdocs/Portfolio` and hit enter. This will move our working directory into the one listed. _But_ because of Wordpress' folder structure, we actually want to go a bit further in. So type this into terminal to go down into the themes folder `cd wp-content/themes` and hit enter. Now we come to installing from this git repository, this is the cool part.

####Cloning this repository

So now that we are in the themes folder in terminal, we want to _'pull down'_ this wordpress boilerplate repository so that we can use it to build our site. So now you have to type this into terminal
```
git clone https://bitbucket.org/shewasonly/wordpress-boilerplate swotheme
```
What this does is, it requests the repository from the remote location (in this instance its bitbucket) pulls down the files and places them in a folder in our working directory (which in our case is current the _themes_ folder). The folder it places it in is called **swotheme** because thats what we asked it to call it, by placing that word after the link. If we had not put that name it would have created the folder with the name 'wordpress-boilerplate'.

Okay cool, now we have the boilerplate files ready(ish) to use.

####Prepping the theme

So now that we have the files we have to do a few more things to really use it. Firstly we have to go back into terminal and move into the theme directory by typing this command: `cd swotheme` (providing you called the theme swotheme).

Now that we are in this directory we need to install all of the node.js packages necessary to run gulp etc. But the cool part is this is all taken care of by the boilerplate, so all we need to do is type this in terminal `sudo npm install` and hit enter (possibly needing to type in your password). This can take a few minutes, but once its done we are ready to get cracking.

Type in `gulp` and hit enter and you are away. (**TIP:** to stop terminal running the gulp command, because its a perpetual command, press `CTRL + C`. That goes for most processes in terminal that you want to stop running)

####Navigating the files

Now that our build tool is working, its good to understand the folder and file structure. There are a lot of comments in the files themselves that should help you understand whats going on but this should help in higher level terms.

- **/dev/** In this folder is all of the files you should work on in regards to CSS (.scss) and JS. This is also where you should be putting images necessary for the theme. They have self explanatory names i.e **/dev/css/** and you should only put relevant files in each of the folders.
- **/dev/js/** Although explained above, this folder does actually have some special rules about it. Any files in the root of this folder will be concatenated into **scripts.js**. If you put in any .js files into the **/dev/js/keep/** folder, they wont be concatenated and it instead will be kept intact and put into **/assets/js/**
- **/includes/funcHelp.php** This file contains some helpers for wordpress functions. Comments can be found in the file but mainly you should know that you don't necessarily have to go into this file, instead you should just use **functions.php**
- **/includes/template/** These are where you should put the templates for the pages. As well as it being good practice, putting them in here in the format they are currently, allows for the use of ajax templating in sites too.

Deployment
------------
When you are happy with what you've done or just want to move the files up with FTP to see in a remote context, you can run another gulp command to move all of the necessary files into a folder called **/build/**. You should then be able to upload just the contents of that folder into whatever server folder you need it to be, without fear of uploading things like the **/node_modules/** folder, which trust me, you don't want to do.

The command to run in terminal is `gulp dist` (and remember if you were running our normal gulp task before, hit `CTRL + C` first, to cancel that)