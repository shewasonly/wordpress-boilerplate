<?php

add_theme_support('post-thumbnails');
add_theme_support('menus');
add_theme_support('html5');

function createPostTypes() {
	global $my_post_types;
	
	foreach($my_post_types as $k => $post)
	{
		$args = array(
			'labels' => array(
				'name' => _x($post['plural'], 'post type general name'),
				'singular_name' => _x($post['single'], 'post type singular name'),
				'add_new' => _x('Add New', strtolower($post['single'])),
				'add_new_item' => __('Add New '.strtolower($post['single'])),
				'edit_item' => __('Edit '.strtolower($post['single'])),
				'new_item' => __('New '.strtolower($post['single'])),
				'view_item' => __('View '.strtolower($post['single'])),
				'search_items' => __('Search '.strtolower($post['plural'])),
				'not_found' =>  __('No '.strtolower($post['plural']).' found'),
				'not_found_in_trash' => __('No '.strtolower($post['plural']).' found in Trash'),
				'parent_item_colon' => '',
			),
			'menu_icon' => $post['icon'],
			'public' => true,
			'rewrite' => array("slug" => strtolower($post['single'])), 
			'supports' => array('editor','title','thumbnail')
		);

		$args = array_merge($args,$post);
	
		register_post_type( strtolower($post['single']) , $args );
	}
}

function createTaxonomies() {
	global $my_taxonomies;
	
	foreach($my_taxonomies as $k => $taxonomy)
	{
		$args = array(
			'hierarchical' => true,
			
			'labels' => array(
				'name' => _x( $taxonomy['plural'], 'taxonomy general name' ),
				'singular_name' => _x( $taxonomy['single'], 'taxonomy singular name' ),
				'search_items' =>  __( 'Search '.$taxonomy['plural'] ),
				'all_items' => __( 'All '.$taxonomy['plural'] ),
				'parent_item' => __( 'Parent '.$taxonomy['single'] ),
				'parent_item_colon' => __( 'Parent '.$taxonomy['single'].':' ),
				'edit_item' => __( 'Edit '.$taxonomy['single'] ),
				'update_item' => __( 'Update '.$taxonomy['single'] ),
				'add_new_item' => __( 'Add New '.$taxonomy['single'] ),
				'new_item_name' => __( 'New '.$taxonomy['single'].' Name' ),
				'menu_name' => __( $taxonomy['plural'] ),
			),
			
			'rewrite' => array(
				'slug' => strtolower($taxonomy['single']),
				'with_front' => true,
				'hierarchical' => true
			),
		);

		$args = array_merge($args,$taxonomy);
		
		register_taxonomy(strtolower($taxonomy['single']), strtolower($taxonomy['post_type']), $args);
	}
}

/*
@function remove_admin_bump

This removes the admin bar when viewing the site's front end and still being
logged in, because I mean come on?
*/
function remove_admin_bump()
{
	remove_action('wp_head', '_admin_bar_bump_cb');
}

add_action('get_header', 'remove_admin_bump');


/*
@function getParallelPost
@var $dir = String('next' or 'prev')
@var $sort = String('date' or 'menu_order')
@var $post = WP Post Object

This function will get the next or previous post no matter what post type your
object is. If you are using post reordering, you will need to use menu_order,
but by default it uses the date of the posts.
*/
function getParallelPost($dir = "next", $sort = "date", $post)
{
	global $wpdb;

	if(empty($post)) return NULL;

	if($dir == "next")
	{
		$dir = "<";
		$dirName = "DESC";
	}
	else if($dir == "prev")
	{
		$dir = ">";
		$dirName = "ASC";
	}

    $query = "SELECT p.* FROM $wpdb->posts AS p";

    $where = "WHERE p.post_type = '".$post->post_type."' AND p.post_status = 'publish'";

    switch($sort)
    {
    	case "menu_order":
    		$where .= " AND p.menu_order ".$dir." ".$post->menu_order;
    		$order =  "ORDER BY p.menu_order ".$dirName;
    		break;
    	case "date":
    	default:
    		$where .= " AND p.post_date ".$dir." '".$post->post_date."'";
    		$order =  "ORDER BY p.post_date ".$dirName;
    		break;
    }
    $order .= " LIMIT 1";

	$result = $wpdb->get_row($query." ".$where." ".$order);
	
	return $result;
}

/*
@function image_crop_dimensions

I certainly call ownership over this unfortunately, but this function is a cool
function that basically makes sure that the image sizes we use are listened to.

If you have an image size that is 1920x1080, and they upload a 1280x720 image, 
this function will scale the image to make sure it still has a 1920x1080 one.

This is because again from a design perspective, its more hassle for an image to
break a layout, than it is to have it display crisply
*/
function image_crop_dimensions($default, $orig_w, $orig_h, $new_w, $new_h, $crop){
    if ( !$crop ) return null; // let the wordpress default function handle this

    $aspect_ratio = $orig_w / $orig_h;
    $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

    $crop_w = round($new_w / $size_ratio);
    $crop_h = round($new_h / $size_ratio);

    $s_x = floor( ($orig_w - $crop_w) / 2 );
    $s_y = floor( ($orig_h - $crop_h) / 2 );

    return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
}

add_filter('image_resize_dimensions', 'image_crop_dimensions', 10, 6);


/*
@function mytheme_setup

This function sets the default image settings in the wordpress backend
to be full size. This was purely from a design perspective more than anything
else, small images inside a post layout looked a heck of a lot worse than 
large images that loaded slow (as stupid as that sounds)
*/

function mytheme_setup() {
	update_option('image_default_align', 'none' );
	update_option('image_default_link_type', 'none' );
	update_option('image_default_size', 'full' );

}
add_action('after_setup_theme', 'mytheme_setup');


/*
@function getPartAjax

This function is the ajax function that gets called by the ajax on the front
end. This shouldn't really need to be tampered with except for if you need
to query with different variables / return different values.
*/

add_action("wp_ajax_getContent", "getPartAjax");
add_action("wp_ajax_nopriv_getContent", "getPartAjax");

function getPartAjax() {
	if(!wp_verify_nonce( $_REQUEST['nonce'], "ajaxHandlerNonce")) {
		exit("No naughty business please");
	}
	$post = get_post($_REQUEST['id']);
	
	$arr 			= getPart($post);
	$arr['debug'] 	= $_REQUEST;
	$arr['active'] 	= determineActive($post);
	
	echo json_encode($arr);
	die();
}

/*
@function getPart
@var $post = WP Post object

This function checks against the current post, to see what is the correct
or most appropriate template to display with. This follows the same rules 
as worpress in terms of hierarchy in choosing files i.e:

{post-type}-{post-slug}.php
single-{post-type}.php
{post-type}.php
*/
function getPart($post)
{

	$content = "";

	switch($post->post_type)
	{
		case "page":

			$file = get_template_directory()."/includes/template/page-".$post->post_name.".php";

			if(file_exists($file))
			{
				require_once $file;
			}
			else
			{
				require_once get_template_directory()."/includes/template/page.php";
			}
			break;
		default:

			$file = get_template_directory()."/includes/template/single-".$post->post_type.".php";

			if(file_exists($file))
			{
				require_once $file;
			}
			else
			{
				require_once get_template_directory()."/includes/template/single.php";
			}
			break;
	}

	$arr = array("content" => $content);

	return $arr;
}

/*
@function ajaxLink
@var $path = String (name of page/post) OR Integer (ID of post)

This handy function, will return to you the necessary formatted string
that you append to links to make them be listened to for the ajax calls.

Usage is like this:

<a <?php echo ajaxLink("home");?>Home page</a>

(or if course being used in a template like this: '<a '.ajaxLink("home").'>Home page</a>')
*/
function ajaxLink($path,$type = "page")
{
	if(is_numeric($path))
	{
		$post = get_post($path);
	}
	else
	{
		$post = get_page_by_path($path, OBJECT, $type);
	}

	if($post)
	{
		$link = get_permalink($post->ID);

		return 'data-ajax data-id="'.$post->ID.'" data-slug="'.$post->post_name.'" data-title="'.$post->post_title.'" href="'.$link.'"';
	}
	else
	{
		return false;
	}
}


?>