<?php
require_once "includes/funcHelp.php";

//--------------------------------------|~~~|
//
//   POST TYPES STAGING AREA
//
//-------------------------------------------
	
	/*
	This is where you can easily register any custom post types.
	There is a helper function inside of funcHelp.php, that you shouldn't 
	need to touch at all, but that takes the array below and runs it through
	wordpress and registers it as a custom post type.
	
	This is a break down of the array to register post types.

	array(
		"plural"	=> "Post_types",      /~/-- If your post type was 'Person' this field you would put 'People'
		"single"	=> "Post_type",       /~/-- Here you would put whatever you want your custom post type
		"icon"		=> "dashicons-menu"   /~/-- Here you put the name of the icon you want to display for your post type
	),	                                     -- You can find all the icons here https://developer.wordpress.org/resource/dashicons/
	
	You can put as many of these arrays in here as you like, and the helper
	function will iterate over all of them and create them.

	In addition to that you can actually add more values to the array, to
	override any of the default arguments that create the post type with
	`register_post_type`.

	So for example, if you wanted to create a post type called 'Person' that
	only supported a title and an image, no content, your array might look
	like this:

	array(
		"plural"	=> "People",
		"single"	=> "Person",
		"icon"		=> "dashicons-groups",
		"supports" 	=> array('title','thumbnail')
	),
	*/
	

	$my_post_types = array(
		array(
			"plural"	=> "Post_types",
			"single"	=> "Post_type",
			"icon"		=> "dashicons-menu"
		),
	);
	
	add_action('init', 'createPostTypes');
	
	/*
	This is where you can easily create custom taxonomies for existing post
	types or the custom post types that you just created.

	The one thing to essentially remember with this, is that in the post_type
	field, you want to put the lower case 'slugified' version of the post type.
	So if your post type is 'Big Data', you'd put in here 'big_data'

	Same goes with this taxonomy helper, that you can actually add any values
	in and it will override the default arguments for the wordpress function
	`register_taxonomy`
	*/

	$my_taxonomies = array(
		array(
			"plural"=>"Examples",
			"single"=>"Example",
			"post_type"=>"post_type"
		),
	);
	
	add_action('init', 'createTaxonomies');


//--------------------------------------|~~~|
//
//   IMAGE SIZE STAGING AREA
//
//-------------------------------------------
	
	/*
	Nothing special here, just add any image sizes you might need for the site
	to generate here. You can put in any other functions to help with the
	theme and it will hook in to wordpress' life cycle and run properly too.
	*/

	function theme_setup() {
		add_image_size('full_screen', 1920, 1080, true);
	}

	add_action( 'after_setup_theme', 'theme_setup' );


//--------------------------------------|~~~|
//
//   GENERAL FUNCTIONS
//
//-------------------------------------------
	
	/*
	This helper function is used to determine what nav item should have
	an active class appended to it.

	At the moment it returns the TITLE of the active nav item, to basically
	match against the loop of nav items in header.php (or wherever)
	*/

	function determineActive($post) {
		$active = "";

		if(isset($post))
		{
			if(is_home()||is_front_page())
			{
				$active = 'Home';
			}
			else if($post->post_title=="About")
			{
				$active = 'Practice';
			}
			else if($post->post_type=="post")
			{
				$active = 'News';
			}
		}
		else
		{
			$active = "Home";
		}

		return $active;
	}


?>