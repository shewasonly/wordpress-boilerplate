

// ===================== 
//
// @function setupHistory
//
// Binds the necessary elements to work with the
// history.js api. Pushes any state changes to the
// getContent function.
//
// =====================

	JTApp.prototype.setupHistory = function() {
		var self = this;

		this.history = window.History;

		this.history.Adapter.bind(window,'statechange',function(){
			var State = self.history.getState();

			if(State.data.url!=null)
			{
				/*
				If google analytics is setup, this can 
				push the ajax call to google analytics

				ga('send', 'pageview', {'page': State.data.slug,'title': State.data.title});
				*/

				self.getContent(State.data);
			}
			else
			{
				self.getContent(null);
			}
		});
	};


// ===================== 
//
// @function getContent
//
// @param data 		: Data that is stored in the history.js state
//
// This function takes the data from the history state change
// and makes the necessary ajax call and returns the data to be used
//
// =====================

	JTApp.prototype.getContent = function(data) {
		var self = this;

		this.transitioning = true;

		var opts = {
			id 		: data.id,
			action	: "getContent",
			nonce	: this.nonce,
			ajax 	: 1
		};

		$.getJSON(this.ajaxURL, opts, function(data)
		{
			self.transitioning = false;
			self.fire();
		});
	};