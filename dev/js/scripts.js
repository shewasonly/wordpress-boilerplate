// ==============================================
// 
// Object
// 
// ==============================================

	var JTApp = function(prop) {
		$.extend(this,prop); // This merges any properties that are passed into the initiation

		this.init(); // Auto initiate the object
	};


// ===================== 
//
// @function init
//
// The init function fires on load, it should only contain
// triggers that get fired once
//
// =====================

	JTApp.prototype.init = function() {

		this.setupVariables();

		if(this.ajaxSite) // Sets up history only if it needs it
		{
			this.setupHistory();
		}

		this.fire();

		this.animationLooper();

		this.setupHeadroom();
	};


// ===================== 
//
// @function setupVariables
//
// Sets up any variables used in the app
//
// =====================

	JTApp.prototype.setupVariables = function() {
		this.debug = false;

		this.ajaxSite = false; // Sets the variable for whether or not this site utilises ajax calls for templates

		if(this.ajaxSite)
		{
			this.transitioning = false;
		}

		this.menu = false // The mobile menu variable
	};


// ===================== 
//
// @function fire
//
// The fire function should contain things that can be
// refired.
//
// =====================

	JTApp.prototype.fire = function() {
		this.events();
		this.dummyImages(); // utilities.js
		this.iframeResize();
	};


// ===================== 
//
// @function events
//
// This function binds any event handlers
//
// =====================

	JTApp.prototype.events = function() {
		var self = this;

		$(window).resize(this.iframeResize);

		if(this.ajaxSite)
		{
			// Bind and unbind the links necessary for the history calls
			$("[data-ajax]").unbind("click");
			$("[data-ajax]").click(function(e) {
				if(history.pushState && e.button==0) // Checks whether the history api is supported and is a left click
				{
					var obj = {
						url: $(this).attr("href"),
						slug: $(this).data("slug"),
						title: $(this).data("title"),
						id: $(this).data("id")
					};

					var actTitle = self.site_name;

					if(obj.title!="")
					{
						actTitle = obj.title + "\u2014" + self.site_name;
					}

					e.preventDefault();
					if(!self.transitioning)
					{
						self.history.pushState(obj, actTitle, obj.url);
					}
					return false;
				}
			});
		}

		// Bind the navicon to open the mobile menu
		$(".navicon").unbind("click");
		$(".navicon").click(function(e) {
			e.preventDefault();
			self.toggleMenu();
			return false;
		});
	};


// ===================== 
//
// @function setupHeadroom
//
// Turns the menu into one using headroom.js
// http://wicky.nillia.ms/headroom.js/
//
// =====================
	
	JTApp.prototype.setupHeadroom = function()
	{
		$("nav#main-head").headroom({
			offset : 200,
			tolerance : {
				up : 5,
				down : 0
			}
		});
	}


// ===================== 
//
// @function toggleMenu
//
// Toggles the opening and closing of the mobile
// menu and anything else that needs to be triggered
// along with it
//
// =====================
	
	JTApp.prototype.toggleMenu = function()
	{
		var self = this;

		if(this.menu)
		{
			this.menu = false;
			$("html").removeClass("mobile-open");
			$("#mobile-menu").fadeOut(500, function() {

			});
		}
		else
		{
			this.menu = true;
			$("html").addClass("mobile-open");
			$("#mobile-menu").fadeIn(500, function() {

			});
		}
	}


// ===================== 
//
// @function animationLooper
//
// Using the requestAnimFrame shim, this loops every time
// the browser allows it to do so
//
// =====================

	JTApp.prototype.animationLooper = function()
	{
		var self = this;

		var scrollT 	= $(window).scrollTop(),
			windH 		= $(window).height(),
			windW 		= $(window).width(),
			scrollB 	= scrollT + windH,
			docH 		= $(document).height();

		requestAnimFrame(this.animationLooper.bind(this));
	};







// ==============================================
// 
// Initiate
// 
// ==============================================

	var app,
		args = {};

	$(document).ready(function() {
		app = new JTApp(args);
	});







