<?php
$templateDir = get_template_directory_uri();
?>
<!DOCTYPE html>
<html>
<head>
    <!-- - - META TAGS - - -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php wp_title("&mdash;",true,"right"); ?><?php echo get_bloginfo("name");?></title>
    <?php
    /* I create the title like this, but admittedly, if you use this in conjunction with
    Yoast's wordpress SEO plugin, you will have to replace the above line with this one:

    <title><?php wp_title();?></title>
    */
    ?>
    <link rel="shortcut icon" href="<?php echo $templateDir;?>/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo $templateDir;?>/favicon.ico" type="image/x-icon">
    <!-- - - END META TAGS - - -->

    <!-- - - SCRIPTS - - -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="<?php echo $templateDir ;?>/assets/js/scripts.js"></script>
    <!-- - - END SCRIPTS - - -->


    <!-- - - STYLESHEETS - - -->
    <link rel="stylesheet" href="<?php echo $templateDir ;?>/assets/css/styles.css">
    <!-- - - END STYLESHEETS - - -->

    <?php
    wp_head();
    ?>

    <?php 
    /*
    These javascript variables are what get passed into and merged into the
    main app object.

    Add any other elements you want passed into the object on load by creating
    another variable like so:

    args.foo = 'bar';
    */
    ?>

    <script>
    args.templateURL    = '<?php echo $templateDir;?>';
    args.siteURL        = '<?php echo home_url();?>';
    args.nonce          = '<?php echo wp_create_nonce("ajaxHandlerNonce");?>';
    args.ajaxURL        = '<?php echo admin_url('admin-ajax.php');?>';
    args.site_name      = '<?php echo get_bloginfo("name");?>';
    </script>
</head>

<body>

    <!--[if lte IE 8]>
    <div id="ie-overlay">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td>
                    <h2>Your browser is outdated</h2>
                    <p>
                        Unfortunately the browser you are using is outdated and you are unable to view our site with it. For a better internet experience please update to the latest version, or even try <a href="https://www.google.com/chrome/browser/desktop/index.html">Google Chrome</a> or <a href="https://www.mozilla.org/en-GB/firefox/new/">Mozilla Firefox</a>
                    </p>
                </td>
            </tr>
        </table>
    </div>
    <![endif]-->

    <?php

    /*
    The theme automatically comes with options for a Nav and a mobile Nav.
    This is simply meant to aid in the speed of creating the theme as these
    are very usual elements that are used in the sites.

    The corresponding .scss file is in dev/css/partials/_nav.scss
    */

    ?>

    <?php
    $active = determineActive($post);
    ?>

    <nav id="main-head">
        <div class="container">
            <a href="#" class="navicon">
                <span class="line one"></span>
                <span class="line two"></span>
                <span class="line three"></span>
            </a>

            <div class="logo_wrap">
                <a href="<?php echo home_url();?>" class="logo"><?php echo get_bloginfo("name");?></a>
            </div>

            <ul class="primary-nav">
            <?php

            /*
            
            A menu will have to be created in the wordpress backend called
            'Main' for this to work. You can find that section in:

            Appearance -> Menus

            */

            $nav_items = wp_get_nav_menu_items("Main");

            foreach($nav_items as $k => $item)
            {
                ?>
                <li>
                    <a href="<?php echo $item->url;?>" class="<?php if($active==$item->title) echo 'active';?>"><?php echo $item->title;?></a>
                </li>
                <?php
            }
            ?>
            </ul>
        </div>
    </nav>

    <div id="mobile-menu">
        <div class="container">
            <ul class="primary-nav">
            <?php
            $nav_items = wp_get_nav_menu_items("Main");

            foreach($nav_items as $k => $item)
            {
                ?>
                <li>
                    <a href="<?php echo $item->url;?>" class="<?php if($active==$item->title) echo 'active';?>"><?php echo $item->title;?></a>
                </li>
                <?php
            }
            ?>
            </ul>
        </div>
    </div>

    <?php
    /*

    This is the end of the automatically created Nav that comes with the
    boilerplate. Feel free to delete it.

    */
    ?>


